FLAG := 
GCC :=g++
TOKENS := ObScanner
SYNTAX := ObParser
BIN    := obc++
SUBDIRS := parser scanner compiler

all:lib $(BIN)

lib: 
	@for n in $(SUBDIRS); do \
	make -C$${n};\
	done
    
$(BIN): Main.C
	$(GCC)  $< -DYYERROR_VERBOSE -I. -Iparser -Iscanner -Icompiler parser/ObParser.o scanner/ObScanner.o compiler/ObCompiler.o -W -Wall -o $(BIN)

clean clear:
	@for n in $(SUBDIRS); do \
	make -C$${n} $(@);\
	done
	rm -Rf *~ *.o $(BIN)

